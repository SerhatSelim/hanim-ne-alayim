import {
  AUTH_LOGIN_SUCCESS,
  AUTH_LOGIN_FAILURE,
  AUTH_LOGOUT
} from '../actions/types'

const initialState = {
  displayName: undefined,
  email: undefined,
  emailVerified: undefined,
  photoURL: undefined,
  isAnonymous: undefined,
  uid: undefined,
  providerData: undefined,
  isAuthenticated: false
}

export const authReducer = (state = initialState, action) => {
  switch(action.type) {
    case AUTH_LOGIN_SUCCESS:
      return {...action.payload, isAuthenticated: true}
    case AUTH_LOGOUT:
      return initialState
    default:
      return state
  }
}