import {
  SHOPPING_LISTS_ADD,
  SHOPPING_LISTS_FETCH,
  SHOPPING_LISTS_REMOVE
} from '../actions/types'

const initialState = {
  shoppingLists: []
}

export const shoppingListsReducer = (state = initialState, action) => {
  switch(action.type) {
    case SHOPPING_LISTS_ADD:
      return {shoppingItems: [...state.shoppingLists, ...action.payload.data]}
    case SHOPPING_LISTS_FETCH:
      return {shoppingLists: action.payload.data}
    default:
      return state
  }
}
