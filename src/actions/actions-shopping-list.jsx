import firebase from 'firebase'
import {
  SHOPPING_ITEM_FETCH,
  SHOPPING_ITEM_ADD,
  SHOPPING_ITEM_REMOVE,
  AUTH_LOGIN_SUCCESS,
  AUTH_LOGIN_FAILURE,
  AUTH_LOGOUT,
  AUTH_REGISTER_SUCCESS
} from '../actions/types'

const firebaseApp = firebase.apps[0]

export const shoppingListFetch = id => dispatch => {
  firebaseApp.database().ref(`list-items/${id}`).on('value', snap => {

    if (snap.val()) {
      const obj = snap.val()
      const keys = Object.keys(obj)

      const values = keys.map(key => ({ key, value: obj[key] }))
      console.log(JSON.stringify(values, null, 2))
      dispatch({ type: SHOPPING_ITEM_FETCH, payload: { data: values } })
    }
    else {
      dispatch({ type: SHOPPING_ITEM_FETCH, payload: { data: [] } })
    }
  })
}

export const shoppingListAdd = (id, value) => dispatch => {
  const key = firebaseApp.database().ref().child('items').push().key

  let item = {done: false, value}
  let updates = {};
  updates[`/items/${key}`] = item
  updates[`/list-items/${id}/${key}`] = item
  firebase.database().ref().update(updates)
}

export const shoppingListRemove = (listId, itemId) => dispatch => {
  firebaseApp.database().ref('items').child(itemId).remove()
  firebaseApp.database().ref(`list-items/${listId}`).child(itemId).remove()
}

export const shoppingListUpdate = (listId, itemId, checked) => dispatch => {
  const item = firebaseApp.database().ref(`/items/${itemId}`)
  let updates = {}
  updates[`/items/${itemId}/done`] = checked
  updates[`/list-items/${listId}/${itemId}/done`] = checked
  firebase.database().ref().update(updates)
}