import firebase from 'firebase'
import { config } from './firebase-app-config'
import {
  SHOPPING_LISTS_ADD,
  SHOPPING_LISTS_REMOVE,
  SHOPPING_LISTS_FETCH
} from './types'

const firebaseApp = firebase.apps[0]

export const shoppingListsFetch = id => dispatch => {
  firebaseApp.database().ref('/list').on('value', snap => {

    if (snap.val()) {
      const obj = snap.val()
      const keys = Object.keys(obj)

      const values = keys.map(key => ({ key, value: obj[key] }))
      dispatch({ type: SHOPPING_LISTS_FETCH, payload: { data: values } })
    }
    else {
      dispatch({ type: SHOPPING_LISTS_FETCH, payload: { data: [] } })
    }
  })
}

export const shoppingListsAdd = item => dispatch => {
  const key = firebaseApp.database().ref().child('/list').push().key

  var updates = {};
  updates[`${key}`] = item
  return firebase.database().ref('/list').update(updates)
}

export const shoppingListsRemove = id => dispatch => {
  firebaseApp.database().ref(`/list-items/${id}`).once('value')
    .then(snap => {
      const obj = snap.val()
      if (obj){
        const keys = Object.keys(obj)
        keys.map(v => firebaseApp.database().ref('/items').child(v).remove())  
      }
    })
  firebaseApp.database().ref('/list').child(id).remove()
  firebaseApp.database().ref('/list-items').child(id).remove()
}
