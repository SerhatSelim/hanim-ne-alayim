export const SHOPPING_ITEM_ADD = 'SHOPPING_ITEM_ADD'
export const SHOPPING_ITEM_REMOVE = 'SHOPPING_ITEM_REMOVE'
export const SHOPPING_ITEM_FETCH = 'SHOPPING_ITEM_FETCH'

export const SHOPPING_LISTS_ADD = 'SHOPPING_LISTS_ADD'
export const SHOPPING_LISTS_REMOVE = 'SHOPPING_LISTS_REMOVE'
export const SHOPPING_LISTS_FETCH = 'SHOPPING_LISTS_FETCH'

export const AUTH_REGISTER_SUCCESS = 'AUTH_REGISTER_SUCCESS'
export const AUTH_LOGIN_SUCCESS = 'AUTH_LOGIN_SUCCESS'
export const AUTH_LOGOUT = 'AUTH_LOGOUT'
export const AUTH_LOGIN_FAILURE = 'AUTH_LOGIN_FAILURE'
