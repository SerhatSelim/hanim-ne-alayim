import React, { Component, Fragment } from 'react';
import AppBar from 'material-ui/AppBar'
import Paper from 'material-ui/Paper'
import Toolbar from 'material-ui/Toolbar'
import Typography from 'material-ui/Typography'
import { withStyles } from 'material-ui/styles'

const styles = theme => ({
  root: {
    width: '100%',
  },
  paper: theme.mixins.gutters({
    paddingTop: 16,
    paddingBottom: 16,
    marginTop: theme.spacing.unit * 3,
  }),
})

class Layout extends Component {
  render() {
    const { classes } = this.props
    return (
      <Fragment>
        <div className={classes.root}>
          <AppBar position="static" color="default">
            <Toolbar>
              <Typography type="title" color="inherit">
                Bizim evin alışveriş listesi
              </Typography>
            </Toolbar>
          </AppBar>
        </div>
        <Paper className={classes.paper} elevation={4}>
          {this.props.children}
        </Paper>
      </Fragment>
    )
  }
}

export default withStyles(styles)(Layout)
