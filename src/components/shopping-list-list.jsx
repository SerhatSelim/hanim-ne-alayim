import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import Card, { CardContent, CardActions } from 'material-ui/Card'
import Grid from 'material-ui/Grid'
import List, { ListItem, ListItemIcon, ListItemText } from 'material-ui/List';
import Button from 'material-ui/Button'
import Typography from 'material-ui/Typography'
import withStyles from 'material-ui/styles/withStyles';
import InboxIcon from 'material-ui-icons/Inbox';
import ShoppingListItem from './shopping-list-item'
import { shoppingListsFetch } from '../actions/actions-shopping-lists'

const styles = theme => ({
  card: {
    minWidth: 275,
  },
  title: {
    marginBottom: 16,
    fontSize: 14,
    color: theme.palette.text.secondary,
  },
  pos: {
    marginBottom: 12,
    color: theme.palette.text.secondary,
  },
  list: {
    width: '100%'
  }
});

class ShoppingListList extends Component {
  componentWillMount(){
    const {shoppingListsFetch} = this.props
    shoppingListsFetch()
  }

  render() {
    const { list, classes, shoppingLists } = this.props

    return (
      <Grid container className={classes.grid}>
        <Grid item xs={12}>
          <List className={classes.list}>
            {
              shoppingLists.map(item =>
                <ShoppingListItem key={item.key} item={item} />
              )
            }
          </List>
        </Grid>
      </Grid>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    shoppingLists: state.shoppingLists.shoppingLists
  }
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      shoppingListsFetch,
    },
    dispatch
  )
}


export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(ShoppingListList))
