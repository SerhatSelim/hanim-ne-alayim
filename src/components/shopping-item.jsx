import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import Icon from 'material-ui/Icon'
import IconButton from 'material-ui/IconButton'
import { ListItem, ListItemText, ListItemIcon } from 'material-ui/List'
import { FormGroup, FormControlLabel } from 'material-ui/Form'
import Checkbox from 'material-ui/Checkbox'
import RemoveShoppingCart from 'material-ui-icons/RemoveShoppingCart'
import { shoppingListRemove, shoppingListUpdate } from '../actions/actions-shopping-list'

class ShoppingItem extends Component {
  handleRemove = () => {
    const {item, listId} = this.props
    shoppingListRemove(listId, item.key)
  }

  handleUpdate = (e) => {
    const {item, listId} = this.props
    this.props.shoppingListUpdate(listId, item.key, e.target.checked)
  }

  render() {
    const {item, listId} = this.props

    return (
      <ListItem>
        <ListItemText primary={item.value.value} />
        <FormControlLabel
            control={
              <Checkbox
                checked={item.value.done}
                onChange={this.handleUpdate}
                value="checkedA"
                label=''
              />
            }
          />
          <IconButton onClick={this.handleRemove}>
            <RemoveShoppingCart />
          </IconButton>
      </ListItem>
    )
  
  }
}


const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      shoppingListRemove,
      shoppingListUpdate
    },
    dispatch
  )
}

export default connect(undefined, mapDispatchToProps)(ShoppingItem)
