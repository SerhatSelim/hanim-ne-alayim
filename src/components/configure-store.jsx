import { createStore, applyMiddleware } from 'redux'
import Thunk from "redux-thunk"

import { rootReducer } from '../reducers/index'

export const configureStore = () => {

  const createStoreWithMiddleware = applyMiddleware(Thunk)(createStore)

  const store = createStoreWithMiddleware(
    rootReducer,
    undefined,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());

  return store;
}
