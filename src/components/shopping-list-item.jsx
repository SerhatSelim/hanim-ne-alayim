import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'

import Icon from 'material-ui/Icon'
import IconButton from 'material-ui/IconButton'
import { ListItem, ListItemText, ListItemIcon } from 'material-ui/List'
import RemoveShoppingCart from 'material-ui-icons/RemoveShoppingCart'
import ModeEdit from 'material-ui-icons/ModeEdit'
import { shoppingListsRemove } from '../actions/actions-shopping-lists'

class ShoppingListItem extends Component {

  render() {
    const { shoppingListsRemove, item } = this.props
    return (
      <ListItem>
        <ListItemText primary={item.value} />
        <ListItemIcon>
          <Link to={`/shopping-list/${item.key}`}>
            <ModeEdit />
          </Link>
        </ListItemIcon>
        <ListItemIcon>
          <IconButton onClick={() => shoppingListsRemove(item.key)}>
            <RemoveShoppingCart />
          </IconButton>
        </ListItemIcon>
      </ListItem>
    )
  }
}


const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      shoppingListsRemove,
    },
    dispatch
  )
}

export default connect(undefined, mapDispatchToProps)(ShoppingListItem)
