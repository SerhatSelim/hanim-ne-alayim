import React, { Fragment } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import  moment from 'moment'

import Button from 'material-ui/Button'
import { withStyles } from 'material-ui/styles'
import AddIcon from 'material-ui-icons/Add'
import Input from 'material-ui/Input'

import withRoot from '../components/withRoot'
import AutoSuggest from '../components/AutoSuggest'
import ShoppingListList from '../components/shopping-list-list'
import Layout from '../components/layout'
import { shoppingListsAdd, shoppingListsFetch } from '../actions/actions-shopping-lists'

const styles = theme => ({
  fab: {
    margin: theme.spacing.unit * 2,
  },
  absolute: {
    flip: false,
    position: 'absolute',
    bottom: 32,
    right: 32,
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: '100%',
  },

})

class Index extends React.Component {
  handleCreateShoppingList = () => {
    moment.locale('tr')
    const time = moment().format("dddd, MMMM Do YYYY, h:mm:ss a")
    this.props.shoppingListsAdd(time)
  }

  render() {
    const { classes } = this.props

    return (
      <Layout>
        <div style={{ display: 'flex' }}>
        </div>
        <ShoppingListList />
        <Button fab color="accent" className={classes.absolute} onClick={this.handleCreateShoppingList}>
          <AddIcon />
        </Button>
      </Layout>
    )
  }
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      shoppingListsFetch,
      shoppingListsAdd
    },
    dispatch
  )
}

export default withRouter(connect(undefined, mapDispatchToProps)(withStyles(styles)(Index)))
