import React, { Component, Fragment } from 'react'
import { Route, Switch } from 'react-router-dom'
import withRoot from '../components/withRoot'
import Login from './login'
import Register from './register'
import ShoppingList from './shopping-list'
import ShoppingLists from './shopping-lists'
import PrivateRoute from '../components/private-route'

class Index extends Component {
  render = () => (
    <Switch>
      <PrivateRoute path='/shopping-lists' component={ShoppingLists} />
      <PrivateRoute path='/shopping-list/:id' component={ShoppingList} />
      <Route path='/login' component={Login} />
      <Route path='/register' component={Register} />
    </Switch>
  )
}

export default withRoot(Index)
