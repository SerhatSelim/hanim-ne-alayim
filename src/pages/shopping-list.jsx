import React, { Fragment } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'

import Button from 'material-ui/Button'
import Save from 'material-ui-icons/ShoppingCart'
import { withStyles } from 'material-ui/styles'

import withRoot from '../components/withRoot'
import AutoSuggest from '../components/AutoSuggest'
import ShoppingList from '../components/shopping-item-list'
import Layout from '../components/layout'
import { shoppingListAdd, shoppingListFetch } from '../actions/actions-shopping-list'

const styles = theme => ({
  root: {
    width: '100%',
  },
  button: {
    margin: theme.spacing.unit,
  },
  paper: theme.mixins.gutters({
    paddingTop: 16,
    paddingBottom: 16,
    marginTop: theme.spacing.unit * 3,
  }),
})

class Index extends React.Component {
  state = {
    value: ''
  }

  componentWillMount() {
    const { shoppingListFetch, match: { params: { id } } } = this.props
    shoppingListFetch(id)
  }

  handleAddToList = () => {
    const { id } = this.props.match.params
    const { value } = this.state

    if (this.state.value) {
      this.props.shoppingListAdd(id, value)
      this.setState({ value: '' })
    }
  }

  handleOnAutoSuggestChange = (value) => {
    this.setState({ value })
  }

  render() {
    const { classes, match: { params: id } } = this.props
    return (
      <Layout>
        <div style={{ display: 'flex' }}>
          <AutoSuggest onChange={this.handleOnAutoSuggestChange} />
          <Button onClick={this.handleAddToList}>Alsın</Button>
        </div>
        <ShoppingList id={id.id} />
        <Button className={classes.button} raised dense>
          <Save className={classes.leftIcon} />
          Save
        </Button>
      </Layout>
    )
  }
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      shoppingListFetch,
      shoppingListAdd
    },
    dispatch
  )
}

export default withRouter(withStyles(styles)(connect(undefined, mapDispatchToProps)(Index)))
